resource "kubernetes_namespace" "argocd" {
  depends_on = [module.gke.name]
  metadata {
    name = "argocd"
  }
}

resource "null_resource" "argocd-apply" {
  depends_on = [kubernetes_namespace.argocd]
  provisioner "local-exec" {
    command = <<-EOT
      gcloud auth activate-service-account --key-file $GOOGLE_APPLICATION_CREDENTIALS
      gcloud container clusters get-credentials ${module.gke.name} --zone ${module.gke.region} --project "${google_project.project.project_id}"
      kubectl apply -n argocd -f "https://raw.githubusercontent.com/argoproj/argo-cd/v2.4.14/manifests/install.yaml"
      kubectl patch -n argocd configmap/argocd-cm --type merge -p '{"data": {"resource.exclusions": "- apiGroups:\n    - cilium.io\n  kinds:\n    - CiliumIdentity\n  clusters:\n    - \"*\"\n"}}'
      kubectl patch svc -n argocd argocd-server -p '{"spec": {"ports": [{"port": 443,"targetPort": 443,"name": "https"},{"port": 80,"targetPort": 80,"name": "http"}],"type": "LoadBalancer"}}'
    EOT
  }
}

resource "kubectl_manifest" "argocd_root" {
  depends_on = [
    null_resource.argocd-apply,
    kubernetes_namespace.argocd,
    module.gke.name
  ]

  yaml_body = <<YAML
  apiVersion: argoproj.io/v1alpha1
  kind: Application
  metadata:
    name: root-app
    namespace: argocd
    finalizers:
    - resources-finalizer.argocd.argoproj.io
  spec:
    destination:
      namespace: argocd
      name: in-cluster
    project: default
    source:
      path: argocd-apps
      repoURL: https://gitlab.com/zencore-dev/argocd-sample-app-of-apps.git
      targetRevision: HEAD
    syncPolicy:
      automated: {}
YAML
}
