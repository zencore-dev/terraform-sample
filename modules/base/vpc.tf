locals {
  vpc_name             = "${lower(var.name)}-vpc"
  router_name          = "${lower(var.name)}-${var.region}-router"
  nat_name             = "${lower(var.name)}-cloud-nat"
  nat_ip               = "${lower(var.name)}-nat-ip"
  subnet_name          = "${lower(var.name)}-subnet"
  pods_subnet_name     = "${lower(var.name)}-gke-pods"
  services_subnet_name = "${lower(var.name)}-gke-services"
  lb_subnet_name       = "${lower(var.name)}-lb-subnet"
}

resource "google_compute_network" "vpc" {
  project                 = google_project.project.project_id
  name                    = local.vpc_name
  auto_create_subnetworks = false
  mtu                     = 1460
  routing_mode            = "GLOBAL"
}

resource "google_compute_subnetwork" "main_subnet" {
  name                     = local.subnet_name
  project                  = google_project.project.project_id
  ip_cidr_range            = var.main_subnet_range
  region                   = var.region
  network                  = google_compute_network.vpc.name
  private_ip_google_access = true
  secondary_ip_range {
    range_name    = local.pods_subnet_name
    ip_cidr_range = var.pods_subnet_range
  }
  secondary_ip_range {
    range_name    = local.services_subnet_name
    ip_cidr_range = var.services_subnet_range
  }
}

resource "google_compute_subnetwork" "lb_subnet" {
  provider      = google-beta
  project       = google_project.project.project_id
  name          = local.lb_subnet_name
  ip_cidr_range = var.lb_subnet_range
  region        = var.region
  purpose       = "INTERNAL_HTTPS_LOAD_BALANCER"
  role          = "ACTIVE"
  network       = google_compute_network.vpc.id
}

resource "google_compute_router" "router" {
  project = google_project.project.project_id
  name    = local.router_name
  region  = google_compute_subnetwork.main_subnet.region
  network = google_compute_network.vpc.id

  bgp {
    asn = 64514
  }
}

#IP NAT Reservation
resource "google_compute_address" "nat_ip" {
  project      = google_project.project.project_id
  name         = local.nat_ip
  address_type = "EXTERNAL"
}

resource "google_compute_router_nat" "cloud_nat" {
  project                            = google_project.project.project_id
  name                               = local.nat_name
  router                             = google_compute_router.router.name
  region                             = google_compute_router.router.region
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.nat_ip.self_link]
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}

### Firewall Rules ###
resource "google_compute_firewall" "iap_allow" {
  project = google_project.project.project_id
  name    = "allow-iap"
  network = google_compute_network.vpc.self_link

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["35.235.240.0/20"]
}

### Private Service Connection Range
resource "google_project_service" "servicenetworking" {
  project                    = google_project.project.id
  service                    = "servicenetworking.googleapis.com"
  disable_dependent_services = true
  disable_on_destroy         = false
}

resource "google_project_service" "redis" {
  project                    = google_project.project.id
  service                    = "redis.googleapis.com"
  disable_dependent_services = true
  disable_on_destroy         = false
}

resource "google_compute_global_address" "service_range" {
  name          = "private-service-address"
  project       = google_project.project.project_id
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  address       = var.psc_range
  prefix_length = 24
  network       = google_compute_network.vpc.id
}

resource "google_service_networking_connection" "private_service_connection" {
  network                 = google_compute_network.vpc.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.service_range.name]

  depends_on = [google_project_service.servicenetworking]
}
