resource "google_redis_instance" "redis_cache" {
  name           = "sample-cache"
  project        = google_project.project.project_id
  tier           = "STANDARD_HA"
  memory_size_gb = 1

  location_id             = "us-west1-b"
  alternative_location_id = "us-west1-c"

  labels = {
    label = "sample-app"
  }

  authorized_network = google_compute_network.vpc.id
  connect_mode       = "PRIVATE_SERVICE_ACCESS"

  redis_version = "REDIS_6_X"
  display_name  = "Sample Redis Instance"

  depends_on = [google_service_networking_connection.private_service_connection]

}

#resource "google_secret_manager_secret" "redis" {
#  secret_id = "sample-redis"
#
#  labels = {
#    label = "sample-app"
#  }
#
#  replication {
#    user_managed {
#      replicas {
#        location = "us-west1"
#      }
#    }
#  }
#}
#
#resource "google_secret_manager_secret_version" "redis-endpoint" {
#  secret = google_secret_manager_secret.redis.id
#
#  secret_data = "redis://${google_redis_instance.redis_cache.host}:${google_redis_instance.redis_cache.port}"
#}
#
resource "kubernetes_secret" "redis-endpoint" {
  depends_on = [module.gke]

  metadata {
    name = "go-redis-redis-addr"
  }

  data = {
    REDIS_ADDR = "${google_redis_instance.redis_cache.host}:${google_redis_instance.redis_cache.port}"
  }
}