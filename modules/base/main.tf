locals {
  project_services = [
    "iam.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
    "servicenetworking.googleapis.com",
    "secretmanager.googleapis.com"
  ]
}
resource "google_project" "project" {
  name            = var.name
  project_id      = var.project_id
  folder_id       = var.root_folder_id
  billing_account = var.billing_account

  auto_create_network = false
}

resource "google_project_iam_binding" "project" {
  project = google_project.project.project_id
  role    = "roles/owner"

  members = [
    "user:skeenan@zencore.dev",
  ]
}

resource "google_project_service" "services" {
  for_each = toset(local.project_services)

  project = google_project.project.id
  service = each.key

  disable_dependent_services = true
}
