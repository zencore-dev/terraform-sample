variable "name" {
  default     = "test"
  description = "Environment Name"
}
variable "project_id" {
  default     = ""
  description = "GCP Project ID"
}
variable "root_folder_id" {
  default     = ""
  description = "GCP Folder ID"
}
variable "seed_project_id" {
  type        = string
  description = "Terraform Seed Project ID"
}
variable "billing_account" {
  description = "Specify the Billing Account ID"
  type        = string
}
variable "region" {
  default     = ""
  description = "Region to deploy resources into"
}

# VPC Variables
variable "main_subnet_range" {
  description = "IP CIDR range for the VPC subnetwork"
  type        = string
}

variable "pods_subnet_range" {
  description = "IP CIDR range for the PODS secondary subnetwork"
  type        = string
}

variable "services_subnet_range" {
  description = "IP CIDR range for the Services secondary subnetwork"
  type        = string
}

variable "lb_subnet_range" {
  description = "IP CIDR range for the LB subnetwork"
  type        = string
}

variable "psc_range" {
  description = "Range for Private Service Connection"
  type        = string
}

# GKE Variables
variable "master_gke_range" {
  description = "Specify the IP range for the GKE Control Plane"
  type        = string
}

variable "master_authorized_networks" {
  type        = list(object({ cidr_block = string, display_name = string }))
  description = "List of master authorized networks. Default to deny all"
  default     = []
}

variable "gke_node_machine_type" {
  description = "Specify the desired machine type"
  type        = string
  default     = ""
}

variable "gke_node_min_count" {
  description = "Set the minimum of nodes"
  type        = number
}

variable "gke_node_max_count" {
  description = "Set the maximum of nodes"
  type        = number
}

variable "max_pods_per_node" {
  description = "Set the max pods per node"
  type        = number
}

variable "disk_size_gb" {
  description = "Specify the desired size for the node disk"
  type        = number
  default     = 100
}

variable "disk_type" {
  description = "Specify the disk type"
  type        = string
  default     = "pd-ssd"
}

variable "initial_node_count" {
  description = "Specify the initial node count"
  type        = string
}

variable "gke_node_spot_enabled" {
  default     = false
  description = "Enables usage of spot instances"
  type        = bool
}

variable "gke_network_tags" {
  type        = list(string)
  default     = []
  description = "Network tags to apply to nodes"
}
