locals {
  main_pool_name = "primary-node-pool"
}

data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

provider "kubectl" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
  load_config_file       = false
}

module "gke" {
  depends_on = [
    google_project.project,
    google_project_service.services["container.googleapis.com"],
    google_project_service.services["servicenetworking.googleapis.com"],
    google_compute_router.router,
    google_compute_router_nat.cloud_nat,
    google_compute_address.nat_ip,
    google_compute_subnetwork.lb_subnet,

  ]

  source                       = "terraform-google-modules/kubernetes-engine/google//modules/beta-private-cluster-update-variant"
  version                      = "22.1.0"
  project_id                   = google_project.project.project_id
  name                         = "${google_project.project.project_id}-gke-main"
  region                       = var.region
  network                      = google_compute_network.vpc.name
  subnetwork                   = google_compute_subnetwork.main_subnet.name
  master_authorized_networks   = var.master_authorized_networks
  master_global_access_enabled = true
  ip_range_pods                = local.pods_subnet_name
  ip_range_services            = local.services_subnet_name
  http_load_balancing          = true
  network_policy               = false
  horizontal_pod_autoscaling   = true
  filestore_csi_driver         = false
  enable_private_endpoint      = false
  enable_private_nodes         = true
  master_ipv4_cidr_block       = var.master_gke_range
  config_connector             = true
  istio                        = false
  cloudrun                     = false
  dns_cache                    = false
  default_max_pods_per_node    = 32
  remove_default_node_pool     = true
  datapath_provider            = "ADVANCED_DATAPATH" # Dataplane V2

  cluster_autoscaling = {
    # NOT needed for node pool autoscaling - this auto-creates new node pools as needed
    enabled : false,
    autoscaling_profile : "BALANCED"
    gpu_resources : [],
    max_cpu_cores : 320000,
    max_memory_gb : 6400000,
    min_cpu_cores : 1,
    min_memory_gb : 1
  }

  node_pools = [
    {
      name                      = local.main_pool_name
      machine_type              = var.gke_node_machine_type
      min_count                 = var.gke_node_min_count
      max_count                 = var.gke_node_max_count
      max_pods_per_node         = var.max_pods_per_node
      local_ssd_count           = 0
      spot                      = var.gke_node_spot_enabled
      local_ssd_ephemeral_count = 0
      disk_size_gb              = var.disk_size_gb
      disk_type                 = var.disk_type
      enable_gcfs               = false
      auto_repair               = true
      auto_upgrade              = true
      autoscaling               = true
      preemptible               = false
      initial_node_count        = var.initial_node_count
      enable_secure_boot        = true
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    (local.main_pool_name) = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }

  node_pools_labels = {
    all = {}

    (local.main_pool_name) = {
      default-node-pool = false
    }
  }

  node_pools_metadata = {
    all = {}

    (local.main_pool_name) = {
      node-pool-metadata-custom-value = local.main_pool_name
    }
  }

  node_pools_tags = {
    all = var.gke_network_tags

    (local.main_pool_name) = [
      local.main_pool_name,
    ]
  }
}
