output "gke_cmd" {
  value = "gcloud container clusters get-credentials ${module.gke.name} --zone ${module.gke.region} --project ${google_project.project.project_id}"
}