root_folder_id  = "121632797988"
region          = "us-west1"
seed_project_id = "sk-sandbox01"
billing_account = "010767-AD0D5D-BCC8F6"

main_subnet_range     = "10.100.64.0/22"
pods_subnet_range     = "10.100.32.0/19"
services_subnet_range = "10.100.0.0/20"
lb_subnet_range       = "10.100.16.0/20"
psc_range             = "10.100.69.0"
master_gke_range      = "10.100.68.0/28"

gke_network_tags      = ["gke-network"]
initial_node_count    = 1 # per zone; node count will be 3x this
gke_node_machine_type = "e2-medium"
gke_node_min_count    = 1   # per zone; node count will be 3x this
gke_node_max_count    = 30  # per zone; node count will be 3x this
max_pods_per_node     = 100 # max 110
gke_node_spot_enabled = true
disk_size_gb          = 50
disk_type             = "pd-standard" # options: pd-standard, pd-ssd, pd-balanced

master_authorized_networks = [
  {
    display_name = "allow-all"
    cidr_block   = "0.0.0.0/0"
  }
]
